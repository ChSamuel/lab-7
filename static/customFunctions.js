function toggleCSS(newcss, cssLinkIndex){
    var inUse = document.getElementsByTagName("link").item(cssLinkIndex);

    var toggledcss = document.createElement("link");
    toggledcss.setAttribute("rel", "stylesheet");
    toggledcss.setAttribute("type", "text/css");
    toggledcss.setAttribute("href", newcss);
    
    document.getElementsByTagName("head").item(0).replaceChild(toggledcss, inUse);
}

function myAccordion(param){
    param.accordion();
}

$(document).ready(function() {
    myAccordion($('#accordion'));
})
