from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Lab7UnitTests(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        invalid_response = Client().get('/this_should_not_exist')
        self.assertEqual(invalid_response.status_code, 404)

    def test_lab6_using_index_function(self):
        foundFunc = resolve('/')
        self.assertEqual(foundFunc.func, index)
